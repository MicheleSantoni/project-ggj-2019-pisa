﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour
{
    public bool isPositioning;
    public bool hasABlock;
    public bool isNearBlock;
    public float speed;
    public GameObject block;
    public GameObject blockPoint;
    private int xInput;
    private int yInput;
    public GameObject levelManager;
    
    // Start is called before the first frame update
    void Start()
    {
        levelManager = GameObject.Find("Level Manager");
        isPositioning = false;
        isNearBlock = false;
        hasABlock = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPositioning)
        {
            GetInputForBlock();
        }
        else
        {
            GetInput();
        }
    }

    private void FixedUpdate()
    {
        if (isPositioning)
        {
            MoveBlock();
        }
        else
        {
            MovePlayer();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Block")
        {
            isNearBlock = true;
            if (!hasABlock)
            {
                block = other.gameObject;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == "Block")
        {
            isNearBlock = false;
            if (!hasABlock)
            {
                block = null;    
            }
        }
    }

    private void GetInputForBlock()
    {
        xInput = Convert.ToInt32(Input.GetKey(KeyCode.D)) -  Convert.ToInt32(Input.GetKey(KeyCode.A));
        yInput = Convert.ToInt32(Input.GetKey(KeyCode.W)) - Convert.ToInt32(Input.GetKey(KeyCode.S));
    }

    private void GetInput()
    {
        xInput = Convert.ToInt32(Input.GetKey(KeyCode.D)) -  Convert.ToInt32(Input.GetKey(KeyCode.A));
        yInput = 0;
    }

    private void MoveBlock()
    {
        Vector3 newPosition = new Vector3(xInput * speed * Time.fixedDeltaTime, yInput * speed * Time.fixedDeltaTime, 0f);
        block.transform.position += newPosition;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            isPositioning = false;
        }

    }

    private void MovePlayer()
    {
        Vector3 newPosition = new Vector3(xInput * speed * Time.fixedDeltaTime, 0f, 0f);
        transform.position += newPosition;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (transform.childCount == 1)
            {
                if (isNearBlock)
                {
                    hasABlock = true;
                    block.transform.parent = transform;
                    block.transform.position = blockPoint.transform.position;
                    block.GetComponent<Rigidbody2D>().simulated = false;
                }    
            }
            else if (transform.childCount > 1)
            {
                Debug.Log("block " + block.name);
                block.transform.parent = null;
                hasABlock = false;
                isPositioning = true;
            }
                            
        }
    }
}
