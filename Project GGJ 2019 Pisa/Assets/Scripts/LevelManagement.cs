﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagement : MonoBehaviour
{
    public GameObject database;
    public GameObject block;
    public GameObject spawn;
    public List<GameObject> blocksPositioned;
    public AudioClip spawnSound;
    public float volumeSpawnSound;
    public float delayTime = 1;
    private float counter = 0;
    private AudioSource audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        SpawnNewBlock();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount == 0)
        {
            if (counter <= delayTime)
            {
                counter += Time.deltaTime;
            }
            else
            {
                SpawnNewBlock();
                counter = 0;
            }
        }
    }

    private void SpawnNewBlock()
    {
        audioSource.PlayOneShot(spawnSound, volumeSpawnSound);
        GameObject instance = Instantiate(block);
        instance.GetComponent<SpriteRenderer>().sprite = database.GetComponent<Database>().shapes[Random.Range(0, database.GetComponent<Database>().shapes.Count)];
        instance.AddComponent<PolygonCollider2D>();
        instance.transform.position = spawn.transform.position;
        instance.transform.parent = transform;
    }

    public void FreeAllBlocks()
    {
        foreach (GameObject block in blocksPositioned)
        {
            block.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            block.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            block.GetComponent<Rigidbody2D>().gravityScale = 1;
            block.layer = 12;
            block.GetComponent<PolygonCollider2D>().isTrigger = false;
        }
    }
}
