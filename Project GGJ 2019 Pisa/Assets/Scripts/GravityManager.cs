﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityManager : MonoBehaviour
{
    public float gravity = 5f;

    private void FixedUpdate()
    {
        //Physics.gravity = new Vector3(0f, gravity, 0f);
        Physics2D.gravity = new Vector2(0f, -gravity);
    }
}
