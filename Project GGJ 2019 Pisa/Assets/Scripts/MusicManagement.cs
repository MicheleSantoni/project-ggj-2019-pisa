﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MusicManagement : MonoBehaviour
{
    public List<AudioClip> musicClips = new List<AudioClip>();
    private AudioSource audioSource;
    [PropertyRange(0, 1)] 
    public float volume = 0.50f;
    [PropertyRange(0, 1)] 
    public float pitch = 0.50f;
    
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!audioSource.isPlaying && musicClips.Count > 0)
        {
            audioSource.pitch = pitch;
            audioSource.clip = musicClips[Random.Range(0, musicClips.Count)];
            audioSource.Play();
        }
    }
}
