﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class UIManagement : MonoBehaviour
{
    public Image arrow;
    [Serializable]
    public class MenuButton
    {
        public Button button;
        public string type;
    }
    public List<MenuButton> buttons = new List<MenuButton>();
    public MenuButton selectedButton;
    public float distance;
    public float shiftDelay = 1f;
    private float timer;
    public bool canShift;
    
    public enum ControllerType{ps4, xbox}
    public ControllerType controllerType;
    // Start is called before the first frame update
    void Start()
    {
        CheckController();
        canShift = true;
        foreach (MenuButton btt in buttons)
        {
            Debug.Log(btt.button.name);
        }
        selectedButton = buttons[0];
        SetArrowPosition();
    }

    // Update is called once per frame
    void Update()
    {
        float input = Input.GetAxis("leftStickY_1");
        if (input != 0f && canShift)
        {
            canShift = false;
            if (input > 0f)
            {
                SwitchSelectedButton(1);
            }
            else
            {
                SwitchSelectedButton(-1);
            }
            SetArrowPosition();
        }

        if (!canShift)
        {
            if (timer < shiftDelay)
            {
                timer += Time.deltaTime;
            }
            else
            {
                canShift = true;
                timer = 0;
            }
        }

        if (controllerType == ControllerType.ps4)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                ControlButtonType();
            }
        }
        else if (controllerType == ControllerType.xbox)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                ControlButtonType();
            }
        }
    }

    private void SetArrowPosition()
    {
        Vector3 newPosition = new Vector3(selectedButton.button.transform.position.x - (selectedButton.button.GetComponent<RectTransform>().rect.width / 2 + distance), selectedButton.button.transform.position.y, selectedButton.button.transform.position.z);
        arrow.transform.position = newPosition;
    }

    private void SwitchSelectedButton(int direction)
    {
        int index;
        if (buttons.IndexOf(selectedButton) + direction < 0)
        {
            index = buttons.Count - 1;
        }
        else if (buttons.IndexOf(selectedButton) + direction > buttons.Count - 1)
        {
            index = 0;
        }
        else
        {
            index = buttons.IndexOf(selectedButton) + direction;
        }
        selectedButton = buttons[index];
    }
    
    private void CheckController()
    {
        string[] names = Input.GetJoystickNames();
		
        if (names[0].Length <= 19)
        {
            controllerType = ControllerType.ps4;
        }
        else
        {
            controllerType = ControllerType.xbox;
        }
    }

    private void ControlButtonType()
    {
        Debug.Log(selectedButton.type);
        if (selectedButton.type == "start")
        {
            SceneManager.LoadSceneAsync(1);
        }
        else if (selectedButton.type == "quit")
        {
            Application.Quit();
        }
    }
}
