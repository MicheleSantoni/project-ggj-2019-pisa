﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Stats_SO : ScriptableObject
{
    public float speedPlayer;
    public float speedPlacing;
    public float rotationSpeed = 5f;
    public Color color;
    public Color cantPlaceColor;
    public int number;
}
