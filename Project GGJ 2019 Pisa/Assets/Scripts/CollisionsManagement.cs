﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Drawing;
using Color = UnityEngine.Color;

[RequireComponent(typeof(AudioSource))]
public class CollisionsManagement : MonoBehaviour
{
    public GameObject smokesVf;
    public AudioClip hitClip;
    [PropertyRange(0, 1)] 
    public float volume = 0.50f;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (hitClip)
        {
            float pitch = Random.Range(1f, 2f);
            Debug.Log(pitch);
            var audioSource = GetComponent<AudioSource>();
            audioSource.pitch = pitch;
            audioSource.PlayOneShot(hitClip, volume);
        }

        Color color = new Color(Random.Range(0, 255), Random.Range(0, 255f), Random.Range(0, 255f));
        smokesVf.GetComponent<SpriteRenderer>().color = color;
        GameObject vfxInstance = Instantiate(smokesVf);
        vfxInstance.transform.position = other.contacts[0].point;
    }
}
