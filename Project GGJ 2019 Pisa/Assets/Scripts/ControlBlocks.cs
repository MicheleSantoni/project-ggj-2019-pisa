﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBlocks : MonoBehaviour
{
    public bool isPositioning;
    public bool hasABlock;
    public bool isNearBlock;
    public bool isRotating;
    public GameObject block;
    public GameObject blockPoint;
    public GameObject levelManager;
    public enum ControllerType{ps4, xbox}
    public ControllerType controllerType;
    public Stats_SO stats;
    private Rigidbody2D rb2D;

    private int number = 0;
    public float rotationTime = 2f;
    private float timer;

    private void Start()
    {
        isPositioning = false;
        isNearBlock = false;
        hasABlock = false;
    }

    private void Update()
    {
        Interaction();
        if (block && isPositioning)
        {
            if (!block.GetComponent<CanPlace>().canPlace)
            {
                block.GetComponent<SpriteRenderer>().color = stats.cantPlaceColor;
            }
            else
            {
                block.GetComponent<SpriteRenderer>().color = stats.color;
            }	
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag("Block"))
        {
	        isNearBlock = true;
            if (!hasABlock && !isPositioning) 
            {
                transform.parent.GetComponent<Player>().block = other.gameObject;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.CompareTag("Block"))
        {
	        isNearBlock = false;
            if (!hasABlock && !isPositioning)
            {
                transform.parent.GetComponent<Player>().block = null;    
            }
        }
    }
    
    private void Interaction()
	{
		if (controllerType == ControllerType.ps4)
		{
			if (stats.number == 1)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button1))
				{
					if (!hasABlock)
					{
						
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning)
				{
					if (Input.GetKey(KeyCode.Joystick1Button4))
					{
						
						SetRotation(1);
					}
					else if (Input.GetKey(KeyCode.Joystick1Button5))
					{
						
						SetRotation(-1);
					}	
				}
			}
			else
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button1))
				{
					if (!hasABlock)
					{
						
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning) 
				{
					if (Input.GetKey(KeyCode.Joystick2Button4))
					{
						
						SetRotation(1);
					}
					else if (Input.GetKey(KeyCode.Joystick2Button5))
					{
						
						SetRotation(-1);
					}	
				}
			}
		}
		else if (controllerType == ControllerType.xbox)
		{
			if (stats.number == 1)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button0))
				{
					if (!hasABlock)
					{
						
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning)
				{
					
					if (Input.GetKey(KeyCode.Joystick1Button4))
					{
						
						timer = 0f;
						SetRotation(1);
					}
					else if (Input.GetKey(KeyCode.Joystick1Button5))
					{
						
						timer = 0f;
						SetRotation(-1);
					}
				}
			}
			else
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button0))
				{
					if (!hasABlock)
					{
						
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning)
				{
					if (Input.GetKey(KeyCode.Joystick2Button4))
					{
						timer = 0f;
						SetRotation(-1);
					}
					else if (Input.GetKey(KeyCode.Joystick2Button5))
					{

						timer = 0f;
						SetRotation(1);
					}
				}
			}
		}
	}
	
	private void TakeBlock()
	{
		if (isNearBlock)
		{
			hasABlock = true;
			block.transform.parent = transform.parent;
			block.transform.position = blockPoint.transform.position;
			block.layer = 9;
			block.GetComponent<PolygonCollider2D>().isTrigger = true;
			block.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
			block.transform.rotation = Quaternion.Euler(Vector2.zero);
			block.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			block.GetComponent<SpriteRenderer>().color = stats.color;
		} 
	}

	private void LeaveBlock()
	{
		block.transform.parent = null;
		block.GetComponent<PolygonCollider2D>().isTrigger = false;
		block.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
		block.GetComponent<Rigidbody2D>().gravityScale = 0;
			
		hasABlock = false;
		isPositioning = true;
		transform.parent.GetComponent<Player>().isPositioning = isPositioning;
	}

	private void SetBlockPosition()
	{
		isPositioning = false;
		transform.parent.GetComponent<Player>().isPositioning = isPositioning;
		block.GetComponent<PolygonCollider2D>().isTrigger = true;
		levelManager.GetComponent<LevelManagement>().blocksPositioned.Add(block);
		block.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
		block.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
		block = null;
	}

	private void SetRotation(int rotationDirection)
	{
		Vector3 newRotation = new Vector3(0f, 0f, block.transform.rotation.eulerAngles.z + stats.rotationSpeed * rotationDirection * Time.fixedDeltaTime);
		block.transform.rotation = Quaternion.Euler(newRotation);
	}
}
