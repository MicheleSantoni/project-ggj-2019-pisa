﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public bool isPositioning;
	public bool hasABlock;
	public bool isNearBlock;
	public bool isRotating;
	public GameObject block;
	public GameObject blockPoint;
	public GameObject levelManager;
	public enum ControllerType{ps4, xbox}
	public ControllerType controllerType;
	public Stats_SO stats;
	private Rigidbody2D rb2D;
	public float direction;
	public AudioClip pickUpSound;
	public AudioClip placeSound;
	public float volumePickUp;
	public float volumePlaceSound;
	private AudioSource audioSource;
	

	private int number = 0;

	private void Start()
	{
		audioSource = GetComponent<AudioSource>();
		levelManager = GameObject.Find("Level Manager");
		rb2D = GetComponent<Rigidbody2D>();
		isPositioning = false;
        isNearBlock = false;
        hasABlock = false;
        isRotating = false;
	}

	void Update()
	{
        if (isPositioning)
        {
	        MoveBlock();
        }
        else
        {
	        Movement();
	        FlipSprite();
	        
        }
		Interaction();
		if (block && isPositioning)
		{
			if (!block.GetComponent<CanPlace>().canPlace)
			{
				block.GetComponent<SpriteRenderer>().color = stats.cantPlaceColor;
			}
			else
			{
				block.GetComponent<SpriteRenderer>().color = stats.color;
			}	
		}
		
	}
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.transform.CompareTag("Block"))
		{
			isNearBlock = true;
			if (!hasABlock && !isPositioning) 
			{
				block = other.gameObject;
			}
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.transform.CompareTag("Block"))
		{
			isNearBlock = false;
			if (!hasABlock && !isPositioning)
			{
				block = null;    
			}
		}
	}
	
	private void Movement()
	{
		if (controllerType == ControllerType.ps4)
		{
			rb2D.MovePosition(rb2D.position + new Vector2(Input.GetAxis("leftStickX_" + stats.number) * stats.speedPlayer * Time.deltaTime, 0));
		}
		else if (controllerType == ControllerType.xbox)
		{
			rb2D.MovePosition(rb2D.position + new Vector2(Input.GetAxis("leftStickX_" + stats.number) * stats.speedPlayer * Time.deltaTime , 0));  
		}
	}

	private void MoveBlock()
	{
		Vector3 newPosX;
		Vector3 newPosY;
		Vector3 newPosition = Vector3.zero;
		Vector2 pos;
		if (controllerType == ControllerType.ps4)
		{
			pos = (new Vector2(Input.GetAxis("leftStickX_" + stats.number), -Input.GetAxis("leftStickY_" + stats.number))).normalized;
			block.GetComponent<Rigidbody2D>().MovePosition(block.GetComponent<Rigidbody2D>().position + pos * stats.speedPlacing * Time.deltaTime);
			
			// newPosX = Vector3.right * Input.GetAxis("rightStickXps4_" + stats.number) * stats.speed * Time.fixedDeltaTime;
			// newPosY = Vector3.up * Input.GetAxis("rightStickYps4_" + stats.number) * stats.speed * Time.fixedDeltaTime;
			// newPosition = newPosX + newPosY;
		}
		else if (controllerType == ControllerType.xbox)
		{
			pos = (new Vector2(Input.GetAxis("leftStickX_" + stats.number), -Input.GetAxis("leftStickY_" + stats.number))).normalized;
			block.GetComponent<Rigidbody2D>().MovePosition(block.GetComponent<Rigidbody2D>().position + pos * stats.speedPlacing * Time.deltaTime);
			//block.GetComponent<Rigidbody2D>().MovePosition(block.GetComponent<Rigidbody2D>().position + new Vector2(Input.GetAxis("leftStickX_" + stats.number) * stats.speed * Time.deltaTime , 0));
			// newPosX = Vector3.right * Input.GetAxis("rightStickXxbox_" + stats.number) * stats.speed * Time.fixedDeltaTime;
			// newPosY = Vector3.up * Input.GetAxis("rightStickYxbox_" + stats.number) * stats.speed * Time.fixedDeltaTime;
			// newPosition = newPosX + newPosY;
		}
		// block.transform.position += newPosition;  
	}

	private void FlipSprite()
	{
		direction = Input.GetAxis("leftStickX_" + stats.number);
		Debug.Log(Input.GetAxis("leftStickX_" + stats.number));
		if (direction > 0f)
		{
			
			GetComponent<SpriteRenderer>().flipX = true;
		}
		else if (direction < 0f)
		{
			GetComponent<SpriteRenderer>().flipX = false;
		}
	}

	private void Interaction()
	{
		if (controllerType == ControllerType.ps4)
		{
			if (stats.number == 1)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button9))
				{
					levelManager.GetComponent<LevelManagement>().FreeAllBlocks();
				}
				if (Input.GetKeyDown(KeyCode.Joystick1Button11))
				{
					if (!hasABlock)
					{
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning)
				{
					if (Input.GetKey(KeyCode.Joystick1Button4))
					{
						
						SetRotation(1);
					}
					else if (Input.GetKey(KeyCode.Joystick1Button5))
					{
						
						SetRotation(-1);
					}	
				}
			}
			else
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button9))
				{
					levelManager.GetComponent<LevelManagement>().FreeAllBlocks();
				}
				if (Input.GetKeyDown(KeyCode.Joystick2Button11))
				{
					if (!hasABlock)
					{
						
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning) 
				{
					if (Input.GetKey(KeyCode.Joystick2Button4))
					{
						
						SetRotation(1);
					}
					else if (Input.GetKey(KeyCode.Joystick2Button5))
					{
						
						SetRotation(-1);
					}	
				}
			}
		}
		else if (controllerType == ControllerType.xbox)
		{
			if (stats.number == 1)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button7))
				{
					levelManager.GetComponent<LevelManagement>().FreeAllBlocks();
				}
				if (Input.GetKeyDown(KeyCode.Joystick1Button9))
				{
					if (!hasABlock)
					{
						
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning)
				{
					
					if (Input.GetKey(KeyCode.Joystick1Button4))
					{
						SetRotation(1);
					}
					else if (Input.GetKey(KeyCode.Joystick1Button5))
					{
						SetRotation(-1);
					}
				}
			}
			else
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button7))
				{
					levelManager.GetComponent<LevelManagement>().FreeAllBlocks();
				}
				if (Input.GetKeyDown(KeyCode.Joystick2Button9))
				{
					if (!hasABlock)
					{
						
						if (isPositioning && block.GetComponent<CanPlace>().canPlace)
						{
							SetBlockPosition();
						}
						else
						{
							TakeBlock();
						}
					}
					else
					{
						LeaveBlock();
					}
				}

				if (isPositioning)
				{
					if (Input.GetKey(KeyCode.Joystick2Button4))
					{
						SetRotation(-1);
					}
					else if (Input.GetKey(KeyCode.Joystick2Button5))
					{
						SetRotation(1);
					}
				}
			}
		}
	}

	private void TakeBlock()
	{
		if (transform.childCount == 1)
		{
			if (isNearBlock)
			{
				audioSource.PlayOneShot(pickUpSound, volumePickUp);
				hasABlock = true;
				block.transform.parent = transform;
				block.transform.position = blockPoint.transform.position;
				block.layer = 9;
				block.GetComponent<PolygonCollider2D>().isTrigger = true;
				block.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
				block.transform.rotation = Quaternion.Euler(Vector2.zero);
				block.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				block.GetComponent<SpriteRenderer>().color = stats.color;
			}    
		}
	}

	public void LeaveBlock()
	{
		if (transform.childCount > 1)
		{
			block.transform.parent = null;
			block.GetComponent<PolygonCollider2D>().isTrigger = false;
			block.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
			block.GetComponent<Rigidbody2D>().gravityScale = 0;
			
			hasABlock = false;
			isPositioning = true;
		}
	}

	private void SetBlockPosition()
	{
		audioSource.PlayOneShot(placeSound, volumePlaceSound);
		isPositioning = false;
		block.GetComponent<PolygonCollider2D>().isTrigger = true;
		levelManager.GetComponent<LevelManagement>().blocksPositioned.Add(block);
		block.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
		block.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
		block = null;
	}

	private void SetRotation(int rotationDirection)
	{
		Vector3 newRotation = new Vector3(0f, 0f, block.transform.rotation.eulerAngles.z + stats.rotationSpeed * rotationDirection * Time.fixedDeltaTime);
		block.transform.rotation = Quaternion.Euler(newRotation);
	}
}
