﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    private Material backMat;
    public float speedBackground;
    void Start()
    {
        backMat = GetComponent<SpriteRenderer>().material;
    }

    void Update()
    {
        backMat.mainTextureOffset += Vector2.left * speedBackground * Time.deltaTime;
    }
}
