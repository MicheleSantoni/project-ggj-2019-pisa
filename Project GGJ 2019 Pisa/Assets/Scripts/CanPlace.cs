﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class CanPlace : MonoBehaviour
{
    public bool canPlace;
    public List<Collider2D> overlappingColliders = new List<Collider2D>();

    private void Start()
    {
        canPlace = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!overlappingColliders.Contains(other))
        {
            overlappingColliders.Add(other);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(overlappingColliders.Contains(other)) 
        {
            overlappingColliders.Remove(other);
        }
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!overlappingColliders.Contains(other.collider))
        {
            overlappingColliders.Add(other.collider);
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if(overlappingColliders.Contains(other.collider)) 
        {
            overlappingColliders.Remove(other.collider);
        }
    }

    private void FixedUpdate()
    {
        //Vector2 point = transform.position;
        //Vector2 size = GetComponent<BoxCollider2D>().size;
//        float angle = transform.rotation.eulerAngles.z;
//        Collider2D[] colliders = Physics2D.OverlapBoxAll(point, size, angle);
//        for (int i = 0; i < colliders.Length; i++)
//        {
//            if ((colliders[i].transform.CompareTag("Block") || colliders[i].transform.CompareTag("ErrorField")) && colliders[i] != GetComponent<BoxCollider2D>())
//            {
//                canPlace = false;
//                break;
//            }
//            canPlace = true;
//        }

        if (overlappingColliders.Count > 0)
        {
            for (int i = 0; i < overlappingColliders.Count; i++)
            {
                if ((overlappingColliders[i].transform.CompareTag("Block")) && overlappingColliders[i] != GetComponent<PolygonCollider2D>())
                {
                    canPlace = false;
                    break;
                }
                canPlace = true;
            }
        }
        else
        {
            canPlace = true;
        }
    }
}
