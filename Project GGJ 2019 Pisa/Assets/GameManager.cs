﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public List<GameObject> players;
    public string[] names;
    
    void Start()
    {
        CheckController();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(1); 
        }
    }

    private void CheckController()
    {
        names = Input.GetJoystickNames();
		
        for (int x = 0; x < names.Length; x++)
        {
            if (names[x].Length <= 19)
            {
                players[x].GetComponent<Player>().controllerType = Player.ControllerType.ps4;
            }
            else
            {
                players[x].GetComponent<Player>().controllerType = Player.ControllerType.xbox;
            }
        }
    }
}
